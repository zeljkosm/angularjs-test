angular.module("testApp")
    .service("vreme", function(){

        function zeroPad(n) { 
            return ("0" + n).slice(-2); 
        }

        this.trenutnoVreme = function(){
            console.log("Getting time");
            var sada = new Date();
            return zeroPad(sada.getHours()) + ":" + zeroPad(sada.getMinutes()) + ":" + zeroPad(sada.getSeconds());
        }

    });