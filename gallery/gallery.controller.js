angular.module("testApp")
    .controller("GalleryController", ["$scope", "$http", function GalleryController($scope, $http){
        $scope.slike = [];
        $scope.loaded = false;

        $http.get("https://api.myjson.com/bins/12qiaa").then(
            function(response) {
                $scope.slike = response.data.galerija;
                $scope.loaded = true;
            }
        );
    }]);