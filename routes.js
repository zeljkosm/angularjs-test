angular.module("testApp")
    .config(function($routeProvider, $locationProvider){

        $locationProvider.hashPrefix("!");

        $routeProvider
            .when("/home", {
                templateUrl: "home/home.template.html",
                controller: "HomeController"
            })
            .when("/gallery", {
                templateUrl: "gallery/gallery.template.html",
                controller: "GalleryController"
            })
            .when("/gallery/:imageID", {
                templateUrl: "image/image.template.html",
                controller: "ImageController"
            })
            .otherwise({
                redirectTo: "/home"
            });

    });