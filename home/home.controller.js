angular.module("testApp")
    .controller("HomeController", ["$scope", "vreme", "$interval", function HomeController($scope, vreme, $interval){
        $scope.podaci = {
            "ime": "Zeljko",
            "prezime": "Smiljanic",
            "email": "zeljkosmilj@gmail.com"
        }
        $scope.trenutnoVreme = "sat se inicijalizuje";
        $scope.sat = $interval(function(){
            $scope.trenutnoVreme = vreme.trenutnoVreme();
        }, 1000);

        $scope.$on('$destroy',function(){
            if($scope.sat) {
                $interval.cancel($scope.sat);
            } 
        });
    }]);
    


