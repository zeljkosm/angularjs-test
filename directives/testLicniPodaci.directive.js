angular.module("testApp")
    .directive("testLicniPodaci", function() {
        return {
            templateUrl: "directives/testLicniPodaci.template.html",
            scope: {
                podaci: "<"
            }
        }
    });